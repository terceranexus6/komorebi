# Green
## version 0.1


Green is a very simple board, based in ATmega328 (same arduino nano), with a led and a moisture sensor. The led will light up if the soil of your pot is dry for you to notice and monitor your pots in a glance. The idea was to create the simplest, cheapest way of developing this idea. The design includes a tag that allows you to write into it.

![](../../../images/green01.png)

The Licence is [GPLv.3](), so you can download, modify and print it. But it you want to buy the soldering pack, or the already finished (no need to solder) pack, please contact us.


