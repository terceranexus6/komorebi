
![](images/logo.png)

Komorebi is a collection of open electronics ideas, focused on improving daily life in a simple, discrete way. In this repo there's a collection of ideas, prototypes, code and pictures with an open licence for you to print, modify and enjoy the designs, as long as you also keep this openess. 

For now, proyects are divided in two categories:

## Wereables

![](images/wereable.gif)

Every piece is hand crafted, hand sewed and uses open source electronics. In this sction expect sewing patterns, electronic tips and ideas for wereable and fashion design, mostly space-opera inspired.

Click [here]() for exploring the projects.

## Plant caring

![](images/plant.gif)

In this section some ideas for garden caring using electronics will be posted and carefully explained. Also some tips on tea brewing.

Click [here]() for exploring the projects.


If you want to know about the shop and the author, please [visit the site]().
