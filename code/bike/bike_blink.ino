/*
  Blinking wereable, this part goes in the bike
  Creator: terceranexus6
  
  LICENCE: GPL v3

  In this case, the inductive coil shall be conected by default in right and left, unless you want to use any other of the 
  analog inputs for convinience (or extra).

  default blinking time is one sec, that could be changed using the RX pin.

  This could work in arduin nano and UNO using the pinout:

  ARDUINO           

  A0                COIL 1 5V 
  A1                COIL 2 5V
  GND               COIL 1 AND COIL 2 GND
  

  but you can also print the PCB, check it here: gitlab.com/terceranexus6/komorebi
*/

//constant values for the board (minimun)

const int led_left=A0;
const int led_right=A1;

//const values for extra lights (extra)
const int extra_led_1=A2;
const int extra_led_2=A3;
const int extra_led_3=A4;

//values for RX and TX digital
int rx = 1;
int tx = 0;


// the setup function runs once when you press reset or power the board
void setup() {
  // initialize pins as an output except from rx.
  pinMode(led_left, OUTPUT);
  pinMode(led_right, OUTPUT);
  
  pinMode(extra_led_1, OUTPUT);
  pinMode(extra_led_2, OUTPUT);
  pinMode(extra_led_3, OUTPUT);
  
  pinMode(rx, INPUT);
  pinMode(tx, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {


  //1 sec delay by default
  int my_time = 1000;

  //reading RX for changing light timing, in case of reading from another arduino
  int timing = digitalRead(rx);

  if ( timing > 0 ) {

    my_time = timing;
    
  }

  //sending the information back to tx for debugging, so you can check if it's either by default or rx
  digitalWrite(tx, my_time);

  //flashing left
  flash_light(led_left, my_time);

  //flashing right
  flash_light(led_right, my_time);

  //flashing extra led 1
  flash_light(extra_led_1, my_time);

  //flashing extra led 2
  flash_light(extra_led_2, my_time);

  //flashing extra led 3
  flash_light(extra_led_3, my_time);
}


void flash_light (const int lgt, int c_time){
  
  analogWrite(lgt, 255);   // turn the LED on (255 is the max voltage level, change for a lower light)
  delay(c_time);             // wait for a second, change for different timing
  analogWrite(lgt, 0);    // turn the LED off by making the voltage LOW
  delay(c_time);
  
  }
