#include "Free_Fonts.h" // Include the header file attached to this sketch

#include "SPI.h"
#include "TFT_eSPI.h"

// Use hardware SPI
TFT_eSPI tft = TFT_eSPI();

unsigned long drawTime = 0;

void setup(void) {

    Serial.begin(115200);
    tft.begin();
    tft.setRotation(1);
    pinMode(A0, INPUT);
    pinMode(A1, INPUT);
    
    //buttons
    pinMode(WIO_KEY_A, INPUT_PULLUP);
    pinMode(WIO_KEY_B, INPUT_PULLUP);
    pinMode(WIO_KEY_C, INPUT_PULLUP);

}

void loop() {

    char tmp[12];
    float sensor_1= analogRead(A0);
    float sensor_2= analogRead(A1);
 

    if (digitalRead(WIO_KEY_A) == LOW) {

      //first mode sensor 1
      tft.setTextSize(1);
      
      tft.fillScreen(TFT_BLACK);
      tft.setTextColor(TFT_WHITE, TFT_BLACK);
  
      // Datum is middle centre
      tft.setTextDatum(MC_DATUM);
      
      tft.drawFloat(sensor_1, 4, 160, 120, 6);
  
      delay(3000);
      
      
    } else if (digitalRead(WIO_KEY_B) == LOW) {
      // second mode sensor 1
      tft.fillRect(0, 80, 320, 80, TFT_BLACK);
      tft.setTextDatum(MC_DATUM);
  
      tft.setTextSize(3);
      
      if ( sensor_1 < 800 ){
        tft.drawString("Wet :)", 160, 120, GFXFF);
      } else {
        tft.drawString("Dry :(", 160, 120, GFXFF);  
      }

      delay(3000);
   
    } else if (digitalRead(WIO_KEY_C) == LOW) {

      //sensor 2 - light
      tft.setTextSize(2);
      
      tft.fillScreen(TFT_BLACK);
      tft.setTextColor(TFT_WHITE, TFT_BLACK);
  
      // Datum is middle centre
      tft.setTextDatum(MC_DATUM);
      
      if ( sensor_2 < 1200 ){
        tft.drawString("I'm not getting", 160, 120, GFXFF);
        delay(2000);
        tft.fillScreen(TFT_BLACK);
        tft.setTextColor(TFT_WHITE, TFT_BLACK);
        
        tft.drawString("enough light", 160, 120, GFXFF);
      } else {
        
        tft.drawString("Yay! light", 160, 120, GFXFF);  
      }
  
      delay(3000);

    } else {
      tft.fillScreen(TFT_BLACK);
      tft.setTextDatum(MC_DATUM);
    
      tft.setTextSize(2);
      tft.drawString("Hi! I'm your plant", 160, 120, GFXFF);   
      delay(2000);
      
      tft.fillScreen(TFT_BLACK);

      if ( sensor_2 < 1200 || sensor_1 < 800  ){
        tft.drawString("I need you", 160, 120, GFXFF);
        delay(2000);   
      }else{
        tft.drawString("I'm happy", 160, 120, GFXFF);
        delay(2000);      
      }
    }


}

// Print the header for a display screen
void header(const char* string, uint16_t color) {
    tft.fillScreen(color);
    tft.setTextSize(1);
    tft.setTextColor(TFT_MAGENTA, TFT_BLUE);
    tft.fillRect(0, 0, 320, 30, TFT_BLUE);
    tft.setTextDatum(TC_DATUM);
    tft.drawString(string, 160, 2, 4); // Font 4 for fast drawing with background
}

// Draw a + mark centred on x,y
void drawDatumMarker(int x, int y) {
    tft.drawLine(x - 5, y, x + 5, y, TFT_GREEN);
    tft.drawLine(x, y - 5, x, y + 5, TFT_GREEN);
}


// There follows a crude way of flagging that this example sketch needs fonts which
// have not been enbabled in the User_Setup.h file inside the TFT_HX8357 library.
//
// These lines produce errors during compile time if settings in User_Setup are not correct
//
// The error will be "does not name a type" but ignore this and read the text between ''
// it will indicate which font or feature needs to be enabled
//
// Either delete all the following lines if you do not want warnings, or change the lines
// to suit your sketch modifications.

#ifndef LOAD_GLCD
    //ERROR_Please_enable_LOAD_GLCD_in_User_Setup
#endif

#ifndef LOAD_FONT2
    //ERROR_Please_enable_LOAD_FONT2_in_User_Setup!
#endif

#ifndef LOAD_FONT4
    //ERROR_Please_enable_LOAD_FONT4_in_User_Setup!
#endif

#ifndef LOAD_FONT6
    //ERROR_Please_enable_LOAD_FONT6_in_User_Setup!
#endif

#ifndef LOAD_FONT7
    //ERROR_Please_enable_LOAD_FONT7_in_User_Setup!
#endif

#ifndef LOAD_FONT8
    //ERROR_Please_enable_LOAD_FONT8_in_User_Setup!
#endif

#ifndef LOAD_GFXFF
    ERROR_Please_enable_LOAD_GFXFF_in_User_Setup!
#endif
